It'll be a simple messenger on Golang that have as fewer dependencies as possible from scratch with the microservice architecture.
It's also my first real try to create microservice app from scratch by myself. 

## Build
### Build protobuf files

```sh
protoc \
  --go_out=./services/auth/internal/domain/ \
  --go_opt=paths=source_relative \
  --go-grpc_out=./services/auth/internal/domain/ \
  --go-grpc_opt=paths=source_relative \
  ./proto/auth.proto && \
protoc \
  --go_out=./services/gateway/internal/repository/ \
  --go_opt=paths=source_relative \
  --go-grpc_out=./services/gateway/internal/repository/ \
  --go-grpc_opt=paths=source_relative \
  ./proto/*
```

### Build services
```sh
# build Auth Service
docker-compose -f ./services/auth/docker-compose.dev.yml build
```

### Build the Gateway
No Gateway yet... TODO

### Run 
```sh
# run Services
docker-compose -f ./services/auth/docker-compose.dev.yml up

# run the Gateway 
... nothing ... yet
```


