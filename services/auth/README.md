# 4nd3rs0n ID
Just my authorization service for my pet projects from scratch

## About
> Maybe some features written here is not working yet. Check TODO.md to see tasks in progress. 

This authorization service has HTTP (REST) and gRPC APIs. It follows the Unix philosophy and KISS rule. Do one thing and do it good -- that's all what this service is about. It doesn't store roles, premissions for your apps, it just stores user's id, login, password hash and ways to restore the user's account. Everything else can store other services for itself

It uses PostgreSQL for storing user's data and Redis to store sessions.

Also this service has API keys and clients whitelist due the security reasons. There is also statistics for the service and hardwer info and healthcheck in the APIs.

If you need to know a little bit more about it's architecture and development setup read DEV.md and read TODO.md to know more about future plans

## Authentication strategies
This service provides only login:password based authentication strategy for now. But, maybe latter, I will implement other types, like multi-factor 

## HTTP API
### New user 
`POST /usr` -- Create new user
Required fields in json body:

- `name` string. 1-50 symbols.
- `login` string. 3-50 symbols. Only a-z 0-9 and _
- `password` string. 6-100 symbols

### Get user
`GET /usr/login-or-id` -- Get user info

### Get me 
`GET /` -- Get info about you

### Login
`POST /login`
Required fields in json body:

- `login` string. 3-50 symbols. Only a-z 0-9 and _
- `password` string. 6-100 symbols

### Logout
`DELETE /logout` -- kills your current session


## Running dev
``` sh
docker-compose -f ./docker-compose.dev.yml up
```

## Running prod
Not ready yet... TODO
