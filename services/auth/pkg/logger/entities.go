package logger

const (
	NICE = iota
	TRC  = iota
	DBG  = iota
	INF  = iota
	WRN  = iota
	ERR  = iota
	FTL  = iota
	// LVLS_COUNT always should be last in the enum
	LVLS_COUNT = iota
)

var ALL = []int{
	NICE, TRC, DBG, INF, WRN, ERR, FTL,
}
var DEFAULT_DEV = []int{
	NICE, DBG, INF, WRN, ERR, FTL,
}
var DEFAULT_PROD = []int{
	NICE, INF, WRN, ERR, FTL,
}
var IMPORTANT_ONLY = []int{
	WRN, ERR, FTL,
}

type ILogger interface {
	Nice(format string, a ...any)
	Trc(format string, a ...any)
	Dbg(format string, a ...any)
	Inf(format string, a ...any)
	Wrn(format string, a ...any)
	Err(format string, a ...any)
	Ftl(format string, a ...any)
}

type LoggerSetup interface {
	//
	SetOutLvl(int) error
	SetLog(file string, lvls ...any) error
	// Output

}
