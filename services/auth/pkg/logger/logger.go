package logger

import (
	"fmt"
	"log"
	"os"
)

// I was too lazy to decide which logger is good.
// So I've basically wrote my own

type middleware func(lvl int, format string, a ...any)

type Logger struct {
	outLvls [LVLS_COUNT]bool
	// TODO: middleware
	// middlewares []middleware

	// Array of strings where logger were used
	// How many soucres will be outputed into the console
}

// Logging
func (l *Logger) log(lvl int, format string, a ...any) (err error) {
	formated := fmt.Sprintf(format, a...)
	// TODO: Run middleware here
	// defer l.Log2File(time.Now().Unix(), lvl, formated)

	if !l.outLvls[lvl] {
		return
	}

	switch lvl {
	case NICE:
		fmt.Printf("\x1b[32m[NICE]\x1b[0m %v\n", formated)
	case TRC:
		fmt.Printf("[TRC]\x1b[0m %v\n", formated)
	case DBG:
		fmt.Printf("[DBG]\x1b[0m %v\n", formated)
	case INF:
		fmt.Printf("\x1b[94m[INF]\x1b[0m %v\n", formated)
	case WRN:
		fmt.Printf("\x1b[33m[WRN]\x1b[0m %v\n", formated)
	case ERR:
		fmt.Printf("\x1b[31m[ERR]\x1b[0m %v\n", formated)
	case FTL:
		log.Fatalf("\x1b[31m[FTL]\x1b[0m %v\b", formated)
	}

	return
}

func (l *Logger) Nice(format string, a ...any) {
	l.log(NICE, format, a...)
}
func (l *Logger) Trc(format string, a ...any) {
	l.log(TRC, format, a...)
}
func (l *Logger) Dbg(format string, a ...any) {
	l.log(DBG, format, a...)
}
func (l *Logger) Inf(format string, a ...any) {
	l.log(INF, format, a...)
}
func (l *Logger) Wrn(format string, a ...any) {
	l.log(WRN, format, a...)
}
func (l *Logger) Err(format string, a ...any) {
	l.log(ERR, format, a...)
}
func (l *Logger) Ftl(format string, a ...any) {
	l.log(FTL, format, a...)
	os.Exit(1)
}

// Log source is the feature to know better
// func (l *Logger) ClearLogSrc() {
// 	l.mu.Lock()
// 	l.logSrc = ""
// 	l.mu.Unlock()
// }
// func (l *Logger) GetLogSrc() string {
// 	var srcsStr string = "Src: "
// 	if len(srcs) > 0 {
// 		srcsStr = "Log src: "
// 		if len(srcs) > depth {
// 			srcsStr += "... "
// 			srcs = srcs[:len(srcs)-depth]
// 		}
// 		for i, src := range srcs {
// 			srcsStr += src
// 			if i != len(srcs)-1 {
// 				srcsStr += " > "
// 			}
// 		}
// 		srcsStr += " | Msg:"
// 	}
// }

// TODO
// Get all resources inforamation. Network speed, cpu load
// func (l *Logger) LogResInfo() {
// }

// TODO
// Write logs to the file
// func (l *Logger) AddWriter(lvls []int, path string) (err error) {
// 	return
// }

// func (l *Logger) Log2File(date int64, lvl int, a any) (err error) {
// 	return
// }

func NewLogger(out ...int) Logger {
	outLvls := [LVLS_COUNT]bool{}

	for i := 0; i < LVLS_COUNT; i++ {
		if !include(out, i) {
			outLvls[i] = false
			continue
		}
		outLvls[i] = true
	}

	return Logger{
		outLvls: outLvls,
	}
}
