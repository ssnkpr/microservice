package logger

import "runtime"

func include[T comparable](arr []T, item T) bool {
	for _, comparable := range arr {
		if comparable == item {
			return true
		}
	}
	return false
}

// Will just trace where your function is called
func trace(lvl int) (file string, line int, funcName string) {
	pc := make([]uintptr, 10) // at least 1 entry needed
	runtime.Callers(2, pc)
	f := runtime.FuncForPC(pc[0])
	file, line = f.FileLine(pc[0])
	funcName = f.Name()

	return
}
