package dbtools

import (
	"fmt"
	"gitlab.com/4nd3rs0n/microservice/services/auth/pkg/logger"
	"os"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

type MigrationOpts struct {
	MSrc    string // Migration source
	DBurl   string //
	VerLim  uint   // Db version limit
	DropDev bool   // Drop DB if STAGE is dev
	Logger  logger.ILogger
}

func DoMigrate(opts MigrationOpts) (ver uint, dirty bool, err error) {
	var m *migrate.Migrate

	opts.Logger.Dbg("Creating new migrate instance")
	m, err = migrate.New(opts.MSrc, opts.DBurl)
	if err != nil {
		return 0, false,
			fmt.Errorf("Failed to initialize migrations %w", err)
	}

	// Drop db if needed, get current db
	if os.Getenv("STAGE") == "dev" && opts.DropDev {
		opts.Logger.Wrn("Stage is dev and dropdev is true. Dropping DB")
		if err = m.Drop(); err != nil {
			return 0, false,
				fmt.Errorf("Failed to drop DB, %w", err)
		}
		// After drop we have to create new migrate instance
		opts.Logger.Dbg("Creating new migrate instance")
		m, err = migrate.New(opts.MSrc, opts.DBurl)
		if err != nil {
			return 0, false,
				fmt.Errorf("Failed to initialize migrations after dropping the DB, %w", err)
		}

	} else {
		// It's strange to check DB version after we drop it
		// So I put version checking into else statement
		opts.Logger.Dbg("Checking current DB version")
		var dirty bool
		ver, dirty, err = m.Version()
		if err != nil && err != migrate.ErrNilVersion {
			return 0, false,
				fmt.Errorf("Failed to check current DB version. Err: %w", err)
		}
		if dirty {
			if err = m.Drop(); err != nil {
				return ver, dirty,
					fmt.Errorf("DB is dirty, but failed to drop it, %w", err)
			}
			// After drop we have to create new migrate instance
			m, err = migrate.New(opts.MSrc, opts.DBurl)
			if err != nil {
				return ver, dirty,
					fmt.Errorf("Failed to initialize migrations after dropping the DB, %w", err)
			}
			// As we dropped DB
			ver = 0
		}
	}

	// Upgrade DB
	var migrateTo string
	if opts.VerLim != 0 && opts.VerLim != ver {
		opts.Logger.Wrn("Upgrading (or downgrading) DB to limit"+
			"(moving from version %d to %s)", ver, opts.VerLim)
		migrateTo = fmt.Sprintf("%d version", opts.VerLim)
		err = m.Migrate(opts.VerLim)
	} else {
		opts.Logger.Inf("Upgrading DB to the latest version")
		migrateTo = "latest version"
		err = m.Up()
	}
	if err != nil {
		if err != migrate.ErrNoChange {
			return 0, false,
				fmt.Errorf("Failed upgrade DB to the %s. Err: %w", migrateTo, err)
		}
		opts.Logger.Inf("DB is already up to date")
	}

	ver, dirty, err = m.Version()

	opts.Logger.Nice("Applying DB migrations complete successful")
	opts.Logger.Inf("DB Info\n\tVersion: %d\n\tDirty: %t", ver, dirty)

	return
}
