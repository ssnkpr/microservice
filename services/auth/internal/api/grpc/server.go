package server

import (
	"context"

	pb "gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/proto"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/sessions"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/users"
)

type server struct {
	pb.UnimplementedAuthServer
	ctx context.Context
	u   users.DbInterface
	s   sessions.DbInterface
}

func (s *server) Reg(ctx context.Context, in *pb.RegReq) (*pb.ErrResp, error) {
	// Check is login exists
	userOpts := users.UserCreationOpts{
		Name: in.GetName(),
		Creds: entities.Creds{
			Login:    in.GetLogin(),
			Password: in.GetPassword(),
		},
	}

	// TODO: Validation

	// Create user
	s.u.SaveUser(ctx, userOpts)

	//
	return &pb.ErrResp{
		ErrCode: 0,
	}, nil
}

func (s *server) Auth(ctx context.Context, in *pb.AuthReq) (*pb.AuthResp, error) {
	lgn := in.GetLogin()
	pwd := in.GetPassword()
	deviceInf := in.GetDeviceInf()

	usrID, valid, err := s.u.CheckCreds(ctx, entities.Creds{
		Login:    lgn,
		Password: pwd,
	})
	if err != nil {
		return nil, err
	}
	if !valid {
		return nil, nil
	}
	sToken, err := s.s.NewSession(ctx, usrID, deviceInf)

	return &pb.AuthResp{
		Token: sToken,
	}, nil
}

func (s *server) CheckAuth(ctx context.Context, in *pb.CheckAuthReq) (*pb.CheckAuthResp, error) {
	tkn := in.GetToken()
	uID, err := s.s.GetUserIdByToken(ctx, tkn)
	if err != nil {
		if err == sessions.ErrSessionNotFound {
			return &pb.CheckAuthResp{
				Userid: "",
			}, nil
		}
		return nil, err
	}
	s.u.GetUserByID(ctx, uID)
	return &pb.CheckAuthResp{
		Userid: uID.String(),
	}, nil
}

// Constructor
func NewServer(ctx context.Context, s sessions.DbInterface, u users.DbInterface) server {
	return server{
		ctx: ctx,
		u:   u,
		s:   s,
	}
}
