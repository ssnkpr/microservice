package server

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/handler"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/middleware"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/sessions"
	"gitlab.com/4nd3rs0n/microservice/services/auth/pkg/logger"
)

type SessionApiOpts struct {
	Sessions sessions.DbInterface
	Logger   logger.ILogger
}

func SessionAPI(ctx context.Context, r chi.Router, opts SessionApiOpts) {
	r.Route("/session", func(r chi.Router) {
		r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			middleware.EnableCORS(&w)

			if r.Method == http.MethodOptions {
				w.WriteHeader(http.StatusOK)
				return
			}
			if r.Method != http.MethodGet {
				w.WriteHeader(http.StatusMethodNotAllowed)
				return
			}

			usrTkn, next, err := middleware.TokenPrehandler(w, r)
			if err != nil {
				opts.Logger.Err("Failed to handle %s %s. Error: %w", r.Method, r.URL, err)
				return
			}
			if !next {
				return
			}

			err = handler.GetAllSessions(ctx, w, usrTkn, opts.Sessions)
			if err != nil {
				opts.Logger.Err("Failed to handle %s %s. Error: %w", r.Method, r.URL, err)
				w.WriteHeader(http.StatusInternalServerError)
			}
		})

		r.HandleFunc("/{tknOrID}", func(w http.ResponseWriter, r *http.Request) {
			middleware.EnableCORS(&w)

			if r.Method == http.MethodOptions {
				w.WriteHeader(http.StatusOK)
				return
			}

			usrTkn, err := middleware.GetAuthToken(r)

			usrTkn, next, err := middleware.TokenPrehandler(w, r)
			if err != nil {
				opts.Logger.Err("Failed to handle %s %s. Error: %w", r.Method, r.URL, err)
				return
			}
			if !next {
				return
			}

			currentUid, err := opts.Sessions.GetUserIdByToken(ctx, usrTkn)
			if err != nil {
				if err == sessions.ErrSessionNotFound {
					w.WriteHeader(http.StatusUnauthorized)
					return
				}
				opts.Logger.Err("Failed to get user by session token: %s", err.Error())
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			var sID uuid.UUID
			reqTknOrSID := chi.URLParam(r, "tknOrID")
			sID, err = uuid.Parse(reqTknOrSID)
			if err != nil {
				sID, err = opts.Sessions.GetSessionIdByToken(ctx, reqTknOrSID)
				if err != nil {
					return
				}
			}

			session, err := opts.Sessions.GetSessionInfoByID(ctx, sID)
			if err != nil {
				opts.Logger.Err(err.Error())
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			if currentUid != session.UID {
				w.WriteHeader(http.StatusForbidden)
				return
			}

			switch r.Method {
			case http.MethodGet:
				session, err := opts.Sessions.GetSessionInfoByID(ctx, sID)
				if err != nil {
					opts.Logger.Err(err.Error())
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
				resp, err := json.Marshal(session)
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
				w.Write(resp)
				w.WriteHeader(http.StatusOK)
				return
			case http.MethodDelete:
				err = opts.Sessions.KillSession(ctx, sID)
				if err != nil {
					opts.Logger.Err("Failed to handle %s %s. Error: %s", r.Method, r.URL, err.Error())
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
				w.WriteHeader(http.StatusNoContent)
				w.Write([]byte("Deleted successfully"))
			default:
				w.WriteHeader(http.StatusMethodNotAllowed)
			}

		})
	})
}
