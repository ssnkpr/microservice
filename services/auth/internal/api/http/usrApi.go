package server

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/handler"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/middleware"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/sessions"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/users"
	"gitlab.com/4nd3rs0n/microservice/services/auth/pkg/logger"
)

type UsrApiOpts struct {
	Users    users.DbInterface
	Sessions sessions.DbInterface
	Logger   logger.ILogger
}

// All user CRUDs are located here

func UsrAPI(ctx context.Context, r chi.Router, opts UsrApiOpts) {
	r.Route("/usr", func(r chi.Router) {
		r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			middleware.EnableCORS(&w)

			if r.Method == http.MethodOptions {
				w.WriteHeader(http.StatusOK)
				return
			}

			// Create new user
			if r.Method == http.MethodPost {
				var in entities.UserApiPostIn
				err := json.NewDecoder(r.Body).Decode(&in)
				if err != nil {
					opts.Logger.Err("Failed to create a new user: failed to handle %s %s. Error: %s", r.Method, r.URL.Path, err.Error())
					w.WriteHeader(http.StatusBadRequest)
					return
				}

				err = handler.CreateUser(ctx, w, in, opts.Users, opts.Sessions)
				if err != nil {
					opts.Logger.Err("Failed to create a new user: failed to handle %s %s. Error: %s", r.Method, r.URL.Path, err.Error())
					w.WriteHeader(http.StatusInternalServerError)
				}
				return
			}

			usrTkn, next, err := middleware.TokenPrehandler(w, r)
			if err != nil {
				opts.Logger.Err("Failed to handle %s %s. Error: %w", r.Method, r.URL, err)
				return
			}
			if !next {
				return
			}
			uID, err := opts.Sessions.GetUserIdByToken(ctx, usrTkn)
			if err != nil {
				if err == sessions.ErrSessionNotFound {
					w.WriteHeader(http.StatusUnauthorized)
					return
				}
				opts.Logger.Err("Failed to get uID by token."+
					" Failed to handle %s %s. Error: %w", r.Method, r.URL, err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			switch r.Method {
			case http.MethodGet:
				err := handler.GetMe(ctx, w, uID, opts.Users)
				if err != nil {
					opts.Logger.Err("Failed to get user."+
						"Failed to handle %s %s. Error: %s", r.Method, r.URL.Path, err.Error())
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
				w.WriteHeader(http.StatusOK)

			case http.MethodDelete: // Delete your account
				err = opts.Users.RemoveUser(ctx, uID)
				if err != nil {
					opts.Logger.Err("Failed to remove user."+
						"Failed to handle %s %s. Error: %w", r.Method, r.URL, err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
				w.WriteHeader(http.StatusNoContent)
				w.Write([]byte("Deleted successfully"))

			default:
				w.WriteHeader(http.StatusMethodNotAllowed)
			}
		})

		r.HandleFunc("/{userID}", func(w http.ResponseWriter, r *http.Request) {
			middleware.EnableCORS(&w)

			if r.Method == http.MethodOptions {
				w.WriteHeader(http.StatusOK)
				return
			}
			if r.Method != http.MethodGet {
				w.WriteHeader(http.StatusMethodNotAllowed)
				return
			}

			usrTkn, next, err := middleware.TokenPrehandler(w, r)
			if err != nil {
				opts.Logger.Err("Failed to handle %s %s. Error: %w", r.Method, r.URL, err)
				return
			}
			if !next {
				return
			}

			reqUID := chi.URLParam(r, "userID")
			reqUIDuuid, err := uuid.Parse(reqUID)
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				w.Write([]byte("Wrong user id in the usr"))
				return
			}

			_, err = opts.Sessions.GetUserIdByToken(ctx, usrTkn)

			if err != nil {
				if err == sessions.ErrSessionNotFound {
					w.WriteHeader(http.StatusUnauthorized)
					return
				}
				opts.Logger.Err("Failed to get uID by token."+
					" Failed to handle %s %s. Error: %s", r.Method, r.URL, err.Error())
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			err = handler.GetUser(ctx, w, reqUIDuuid, opts.Users)

			if err != nil {
				opts.Logger.Err("Failed to handle %s %s. Error: %w", r.Method, r.URL, err)
				w.WriteHeader(503)
			}
		})
	})
}
