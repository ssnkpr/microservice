package server

import (
	"context"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/sessions"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/users"
	"gitlab.com/4nd3rs0n/microservice/services/auth/pkg/logger"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

type HttpApiOpts struct {
	ApiPrefix string
	Users     users.DbInterface
	Sessions  sessions.DbInterface
	Logger    logger.ILogger
}

func NewApi(ctx context.Context, opts HttpApiOpts) (r *chi.Mux, err error) {
	r = chi.NewRouter()

	r.Use(middleware.Logger)

	uApiOpts := UsrApiOpts{
		Users:    opts.Users,
		Sessions: opts.Sessions,
		Logger:   opts.Logger,
	}
	sApiOpts := SessionApiOpts{
		Sessions: opts.Sessions,
		Logger:   opts.Logger,
	}
	lApiOpts := LoginApiOpts{
		Users:    opts.Users,
		Sessions: opts.Sessions,
		Logger:   opts.Logger,
	}

	r.Route(opts.ApiPrefix, func(w chi.Router) {
		UsrAPI(ctx, r, uApiOpts)
		SessionAPI(ctx, r, sApiOpts)
		LoginAPI(ctx, r, lApiOpts)
	})

	return
}
