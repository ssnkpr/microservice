package handler

import (
	"context"
	"net/http"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/sessions"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/users"
)

// Handles DELETE or GET request. Removing current use session
func Logout(ctx context.Context, w http.ResponseWriter, token string,
	s sessions.DbInterface) {
	sID, err := s.GetSessionIdByToken(ctx, token)
	if err != nil {
		if err == sessions.ErrSessionNotFound {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("Session with this token doesn't exist"))
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	err = s.KillSession(ctx, sID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Session were not killed. Most probably it doesn't exist."))
		return
	}

	w.WriteHeader(http.StatusOK)
}

// Handles POST request. Creating new session for users
func Login(ctx context.Context, w http.ResponseWriter, in entities.Creds,
	u users.DbInterface, s sessions.DbInterface) {

	uID, valid, err := u.CheckCreds(ctx, in)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !valid {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte("Login or password is invalid"))
		return
	}

	// TODO: Handle device info
	sTkn, err := s.NewSession(ctx, uID, "")

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(sTkn))
}
