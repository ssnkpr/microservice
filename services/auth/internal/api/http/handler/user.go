package handler

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/sessions"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/users"
)

type CUsrResp struct {
	Token string `json:"token"`
}

func CreateUser(ctx context.Context, w http.ResponseWriter, in entities.UserApiPostIn,
	u users.DbInterface, s sessions.DbInterface) error {

	creationOpts := users.UserCreationOpts{
		Name: in.Name,
		Creds: entities.Creds{
			Login:    in.Login,
			Password: in.Password,
		},
	}

	uid, err := u.SaveUser(ctx, creationOpts)
	if err != nil {
		if err == users.ErrLoginTaken {
			w.WriteHeader(http.StatusConflict)
			w.Write([]byte("This login is already taken. Try another"))
			return nil
		}
		return err
	}

	sessionToken, err := s.NewSession(ctx, uid, "")
	if err != nil {
		return err
	}

	resp := CUsrResp{
		Token: sessionToken,
	}
	respJson, err := json.Marshal(resp)
	if err != nil {
		return err
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	w.Write(respJson)
	return nil
}

func GetUser(ctx context.Context, w http.ResponseWriter, reqUID uuid.UUID,
	u users.DbInterface) (err error) {

	return
}

type GetMeResp struct {
	UsrID string `json:"id"`
	Login string `json:"login"`
	Name  string `json:"name"`
}

// Handles GET request to /usr
func GetMe(ctx context.Context, w http.ResponseWriter, uID uuid.UUID,
	u users.DbInterface) (err error) {

	user, err := u.GetUserByID(ctx, uID)
	if err != nil {
		return err
	}
	respStruct := GetMeResp{
		UsrID: user.ID.String(),
		Login: user.Login,
		Name:  user.Name,
	}

	resp, err := json.Marshal(respStruct)
	if err != nil {
		return err
	}

	w.WriteHeader(http.StatusOK)
	w.Write(resp)

	return nil
}
