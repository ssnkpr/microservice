package handler

import (
	"context"
	"encoding/json"
	"net/http"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/sessions"
)

// Handles GET request that returns use
func GetAllSessions(ctx context.Context, w http.ResponseWriter, usrTkn string,
	s sessions.DbInterface) (err error) {
	uID, err := s.GetUserIdByToken(ctx, usrTkn)
	if err != nil {
		if err == sessions.ErrSessionNotFound {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		return
	}

	usrSessions, err := s.GetAllSessions(ctx, uID)
	if err != nil {
		return
	}

	resp, err := json.Marshal(usrSessions)
	if err != nil {
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(resp)
	return
}
