package server

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/handler"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http/middleware"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/sessions"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/users"
	"gitlab.com/4nd3rs0n/microservice/services/auth/pkg/logger"
)

type LoginApiOpts struct {
	Users    users.DbInterface
	Sessions sessions.DbInterface
	Logger   logger.ILogger
}

func LoginAPI(ctx context.Context, r chi.Router, opts LoginApiOpts) {
	r.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		// Middlewares
		middleware.EnableCORS(&w)

		switch r.Method {
		case http.MethodOptions:
			w.WriteHeader(http.StatusOK)
		case http.MethodPost:
			// Parse creds
			var creds entities.Creds
			err := json.NewDecoder(r.Body).Decode(&creds)
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			handler.Login(ctx, w, creds, opts.Users, opts.Sessions)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	})

	r.HandleFunc("/logout", func(w http.ResponseWriter, r *http.Request) {
		// Middlewares
		middleware.EnableCORS(&w)
		if r.Method == http.MethodOptions {
			w.WriteHeader(http.StatusOK)
			return
		}
		// I didn't decide which method is more correct to use
		// so I'll use both: GET and DELETE :D
		if r.Method != http.MethodGet && r.Method != http.MethodDelete {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		usrTkn, next, err := middleware.TokenPrehandler(w, r)
		if err != nil {
			opts.Logger.Err("Failed to handle %s %s. Error: %w", r.Method, r.URL, err)
			return
		}
		if !next {
			return
		}

		handler.Logout(ctx, w, usrTkn, opts.Sessions)
	})
}
