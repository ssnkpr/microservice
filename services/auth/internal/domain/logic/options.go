package logic

// Here located function arguments types

type UserCreationOpts struct {
	// User info
	Name            string
	Login, Password string
	// Session info
	DeviceInfo string
	IpAddr     string
}
