package logic

import (
	"context"
	"errors"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/sessions"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/users"

	"github.com/go-playground/validator/v10"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/dto"
)

type CreateUserResp struct {
	SessionToken string
}

// Creates new user, returns session token to manipulate this user
func CreateUser(
	ctx context.Context,
	user dto.CreateUserOpts,
	udbi users.DbInterface,
	sdbi sessions.DbInterface,
	validate validator.Validate) (CreateUserResp, error) {

	err := validate.Struct(user)

	var resp CreateUserResp

	saveOpts := users.UserCreationOpts{
		Name: user.Name,
		Creds: entities.Creds{
			Login:    user.Login,
			Password: user.Password,
		},
	}
	uID, err := udbi.SaveUser(ctx, saveOpts)
	if err != nil {
		return resp, err
	}
	sessionToken, err := sdbi.NewSession(ctx, uID, user.DeviceInfo)
	if err != nil {
		return resp, err
	}

	resp.SessionToken = sessionToken
	return resp, nil
}

// Removes user. If user deleted -- returns nil, otherwise error
func RemoveUser(
	ctx context.Context,
	opts dto.RemoveUserOpts,
	udbi users.DbInterface,
	sdbi sessions.DbInterface) error {

	uID, err := sdbi.GetUserIdByToken(ctx, opts.Token)
	if err != nil {
		return err
	}
	sameUser := uID == opts.UID
	// TODO: Admins table in DB and admin check
	admin := false
	if !sameUser && !admin {
		return errors.New("Premission denied")
	}

	err = udbi.RemoveUser(ctx, opts.UID)
	if err != nil {
		return err
	}
	_, err = sdbi.KillAllSessions(ctx, opts.UID)
	if err != nil {
		return err
	}
	return nil
}

func GetUser(
	ctx context.Context,
	opts dto.GetUserOpts,
	udbi users.DbInterface,
	sdbi sessions.DbInterface) error {

	uID, err := sdbi.GetUserIdByToken(ctx, opts.Token)
	if err != nil {
		return err
	}
	sameUser := uID == opts.UID
	// TODO: Admins table in DB and admin check
	admin := false
	if !sameUser && !admin {
		return errors.New("Premission denied")
	}
	return nil
}

type GetMeOpts struct {
	Token string
}

func GetMe(
	ctx context.Context,
	opts GetMeOpts,
	udbi users.DbInterface,
	sdbi sessions.DbInterface) error {

	uid, err := sdbi.GetUserIdByToken(ctx, opts.Token)
	if err != nil {
		return err
	}

	getUserOpts := dto.GetUserOpts{
		UID:   uid,
		Token: opts.Token,
	}
	GetUser(ctx, getUserOpts, udbi, sdbi)

	return nil
}
