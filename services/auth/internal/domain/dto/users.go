package dto

import "github.com/google/uuid"

// Options
type CreateUserOpts struct {
	// User info
	Name     string `validator:"min=3,max=40,regexp=^[a-zA-Z]$"`
	Login    string `validator:"min=3,max=40,regexp=^[a-zA-Z0-9][a-zA-Z0-9_]*$"`
	Password string `validator:"min=6"`
	// Session info
	DeviceInfo string
	IpAddr     string `validator:"ip"`
}

type RemoveUserOpts struct {
	UID   uuid.UUID
	Token string `validator:"len=64"`
}

type GetUserOpts struct {
	UID   uuid.UUID
	Token string `validator:"len=64"`
}

type GetMeOpts struct {
	Token string `validator:"len=64"`
}

// TODO
type ChangeNameOpts struct {
}
type ChangeLoginOpts struct {
}
type ChangePasswordOpts struct {
}
