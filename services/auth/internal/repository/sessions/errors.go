package sessions

import (
	"errors"
	"fmt"
)

var ErrTokenLen = fmt.Errorf("Token length should be equal to %d", TOKEN_LENGTH)
var ErrSessionNotFound = errors.New("Session wasn't found")
