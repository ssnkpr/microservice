package sessions

import (
	"context"
	"crypto"
	cryptoRand "crypto/rand"
	_ "crypto/sha256"
	"encoding/hex"
	"errors"
	"strconv"
	"time"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"

	"github.com/google/uuid"
	redis "github.com/redis/go-redis/v9"
)

// TODO: Make sessions to contain the following information:
// DONE token: <string>
// DONE device: <string>
// last use ip: <255.255.255.255>
// last use: <date><time>
// DONE created by ip: <255.255.255.255>
// DONE created at: <date><time>
// expire at: <date><time>

func HashToken(tkn string) (tokenHash string, err error) {
	if len([]byte(tkn)) != TOKEN_LENGTH {
		return "", ErrTokenLen
	}
	tokenBytes := make([]byte, TOKEN_BYTES_LENGTH, TOKEN_BYTES_LENGTH)
	hex.Decode(tokenBytes, []byte(tkn))

	sha := crypto.SHA256.New()
	sha.Write(tokenBytes)
	hashTokenBytes := sha.Sum(nil)
	tokenHashStr := hex.EncodeToString(hashTokenBytes)

	return tokenHashStr, nil
}

type RedisInterface struct {
	Client *redis.Client
}

func NewRedisInterface(client *redis.Client) DbInterface {
	return &RedisInterface{
		Client: client,
	}
}

// Creates new session for the user, returns the token to use this session or an error
func (ri *RedisInterface) NewSession(ctx context.Context, usrId uuid.UUID, deviceInf string) (string, error) {
	const nilToken string = ""
	// Generate random bytes
	tokenBytes := make([]byte, TOKEN_BYTES_LENGTH, TOKEN_BYTES_LENGTH)
	_, err := cryptoRand.Read(tokenBytes)
	if err != nil {
		return nilToken, err
	}
	// By the security reasons we are storing in redis
	// hashes of the tokens
	// But I don't want to use bcrypt or argon2
	// due the perfoemance reasons
	sha := crypto.SHA256.New()
	sha.Write(tokenBytes)
	hashTokenBytes := sha.Sum(nil)
	hashTokenStr := hex.EncodeToString(hashTokenBytes)

	var sessionID uuid.UUID = uuid.New()
	sessionData := map[string]interface{}{
		// Session Token hash.
		// Needed in the session data to be able to
		// remove token from chache when killing session
		"sth": hashTokenStr,
		// User Id
		"uid": usrId.String(),
		// Created at
		"cat": time.Now().UTC().Unix(),
		// Device information
		"dinf": deviceInf,
		// TODO: use the real ip
		// Created from the IP
		"cfip": "127.0.0.1",
	}
	// Data associated with token hash
	dawth := map[string]interface{}{
		// Session ID. Can be used to get detail inforamtion about the
		"sid": sessionID.String(),
		"uid": usrId.String(),
	}

	_, err = (*ri.Client).Pipelined(ctx, func(pipe redis.Pipeliner) error {
		var err error
		// Map session data with session ID
		// Session ID allows to manipulate the session
		err = pipe.HSet(ctx, "sessions.id."+sessionID.String(), sessionData).Err()
		if err != nil {
			return err
		}
		// Map session token hash with the session ID and userID
		// Used to grant or deny access latter based on Authorization header
		err = pipe.HSet(ctx, "sessions.token."+hashTokenStr, dawth).Err()
		if err != nil {
			return err
		}
		// Map user's ID with session token hash
		// Used to get all the session that are related to the user
		err = pipe.SAdd(ctx, "users."+usrId.String()+".sessions", sessionID.String()).Err()
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return nilToken, err
	}
	token := hex.EncodeToString(tokenBytes)
	return token, nil
}

func (ri *RedisInterface) KillSession(ctx context.Context, sID uuid.UUID) error {
	_, err := (*ri.Client).Pipelined(ctx, func(pipe redis.Pipeliner) error {
		tokenHash, err := (*ri.Client).HGet(ctx, "sessions.id."+sID.String(), "sth").Result()
		if err != nil {
			if err == redis.Nil {
				return ErrSessionNotFound
			}
			return err
		}
		uIDstr, err := (*ri.Client).HGet(ctx, "sessions.id."+sID.String(), "uid").Result()
		if err != nil {
			return err
		}
		uID, err := uuid.Parse(uIDstr)
		if err != nil {
			return err
		}
		err = (*ri.Client).Del(ctx, "sessions.id."+sID.String()).Err()
		if err != nil && err != redis.Nil {
			return err
		}
		err = (*ri.Client).Del(ctx, "sessions.token."+tokenHash).Err()
		if err != nil && err != redis.Nil {
			return err
		}
		err = (*ri.Client).SRem(ctx, "users."+uID.String()+".sessions", sID.String()).Err()
		if err != nil {
			return err
		}

		return nil
	})

	return err
}

func (ri *RedisInterface) GetSessionIdByToken(ctx context.Context, tkn string) (uuid.UUID, error) {
	tokenHashStr, err := HashToken(tkn)
	if err != nil {
		return uuid.Nil, err
	}

	sIDstr, err := (*ri.Client).HGet(ctx, "sessions.token."+tokenHashStr, "sid").Result()
	if err != nil {
		if err == redis.Nil {
			return uuid.Nil, ErrSessionNotFound
		}
		return uuid.Nil, err
	}

	sessionID, err := uuid.Parse(sIDstr)
	return sessionID, err
}

func (ri *RedisInterface) KillAllSessions(ctx context.Context, usrId uuid.UUID) (int, error) {
	var killed int = 0
	result, err := (*ri.Client).SMembers(ctx, "users."+usrId.String()+".sessions").Result()
	if err != nil {
		return killed, err
	}

	for _, sIDstr := range result {
		tokenHash, err := (*ri.Client).HGet(ctx, "sessions.id."+sIDstr, "sth").Result()
		if err != nil {
			return killed, err
		}
		err = (*ri.Client).Del(ctx, "sessions.id."+sIDstr).Err()
		if err != nil && err != redis.Nil {
			return killed, err
		}
		err = (*ri.Client).Del(ctx, "sessions.token."+tokenHash).Err()
		if err != nil && err != redis.Nil {
			return killed, err
		}
		err = (*ri.Client).SRem(ctx, "users."+usrId.String()+".sessions", sIDstr).Err()
		if err != nil {
			return killed, err
		}

		killed++
	}
	return killed, nil
}

func (ri *RedisInterface) GetAllSessions(ctx context.Context, uID uuid.UUID) ([]entities.Session, error) {
	var sessions []entities.Session

	result, err := (*ri.Client).SMembers(ctx, "users."+uID.String()+".sessions").Result()

	if err != nil {
		return sessions, err
	}

	for _, sIDstr := range result {
		sID, err := uuid.Parse(sIDstr)
		if err != nil {
			return sessions, err
		}
		session, err := ri.GetSessionInfoByID(ctx, sID)
		if err != nil {
			return sessions, err
		}
		sessions = append(sessions, session)
	}

	return sessions, nil
}

func (ri *RedisInterface) GetSessionInfoByToken(ctx context.Context, token string) (entities.Session, error) {
	sID, err := ri.GetSessionIdByToken(ctx, token)
	if err != nil {
		return entities.Session{}, err
	}
	session, err := ri.GetSessionInfoByID(ctx, sID)
	return session, err
}

func (ri *RedisInterface) GetSessionInfoByID(ctx context.Context, sessionID uuid.UUID) (entities.Session, error) {
	var sInf entities.Session = entities.Session{}

	mapCmd, err := (*ri.Client).HGetAll(ctx, "sessions.id."+sessionID.String()).Result()
	if err != nil {
		if err == redis.Nil {
			return sInf, ErrSessionNotFound
		}
		return sInf, err
	}

	// Checking are values exists
	uidStr, exists := mapCmd["uid"]
	if !exists {
		return sInf, errors.New("Value of uid wasn't found in this session. Probably this session doesn't exist")
	}
	createdAtStr, exists := mapCmd["cat"]
	if !exists {
		return sInf, errors.New("Value of cat wasn't found in this session.")
	}
	deviceInf, exists := mapCmd["dinf"]
	if !exists {
		return sInf, errors.New("Value of dinf wasn't found in this session.")
	}
	creatorIP, exists := mapCmd["cfip"]
	if !exists {
		return sInf, errors.New("Value of cfip wasn't found in this session.")
	}

	// Parsing values
	uid, err := uuid.Parse(uidStr)
	if err != nil {
		return sInf, err
	}
	createdAt, err := strconv.ParseInt(createdAtStr, 10, 64)
	if err != nil {
		return sInf, err
	}

	sInf = entities.Session{
		SID:       sessionID,
		UID:       uid,
		CreatedAt: createdAt,
		DeviceInf: deviceInf,
		CreatorIP: creatorIP,
	}

	return sInf, err
}

func (ri *RedisInterface) GetUserIdByToken(ctx context.Context, tkn string) (uuid.UUID, error) {
	tokenHashStr, err := HashToken(tkn)
	if err != nil {
		return uuid.Nil, err
	}

	uIDstr, err := (*ri.Client).HGet(ctx, "sessions.token."+tokenHashStr, "uid").Result()
	if err != nil {
		if err == redis.Nil {
			return uuid.Nil, ErrSessionNotFound
		}
		return uuid.Nil, err
	}

	usrID, err := uuid.Parse(uIDstr)
	return usrID, err
}
