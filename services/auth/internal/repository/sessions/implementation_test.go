package sessions_test

import (
	"context"
	"encoding/hex"
	"fmt"
	"math/rand"
	"regexp"
	"testing"

	"github.com/go-redis/redismock/v9"
	"github.com/google/uuid"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/sessions"
)

func TestNewSession(t *testing.T) {
	db, mock := redismock.NewClientMock()
	ri := sessions.NewRedisInterface(db)

	// TODO: More than one testcase
	t.Run("New session", func(t *testing.T) {
		uID := uuid.New()
		dInf := "Apple pro"

		sessionData := map[string]interface{}{
			// Any 64 symbols
			"sth": `^.{64}$`,
			"uid": uID.String(),
			// Any Int64 regex
			"cat":  `^-?[0-9]{1,19}$`,
			"dinf": dInf,
			"cfip": "127.0.0.1",
		}
		dawth := map[string]interface{}{
			"sid": `^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$`,
			"uid": uID.String(),
		}

		mock.Regexp().ExpectHSet(`^sessions\.id\.[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$`,
			sessionData).SetVal(1)
		mock.Regexp().ExpectHSet(`^sessions\.token\..{64}$`, dawth).SetVal(1)
		mock.Regexp().ExpectSAdd("users."+uID.String()+".sessions",
			`^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$`).SetVal(1)

		token, err := ri.NewSession(context.Background(), uID, dInf)
		if err != nil {
			t.Error(err)
		}
		regex := regexp.MustCompile(`^.{64}$`)
		if !regex.MatchString(token) {
			t.Error("Token doesn't match regex")
		}
	})
}

func TestKillSession(t *testing.T) {
	db, mock := redismock.NewClientMock()
	ri := sessions.NewRedisInterface(db)

	t.Run("Kill existing session", func(t *testing.T) {
		tknBytes := make([]byte, sessions.TOKEN_BYTES_LENGTH, sessions.TOKEN_BYTES_LENGTH)
		_, err := rand.Read(tknBytes)
		if err != nil {
			t.Error(err)
		}
		tknStr := hex.EncodeToString(tknBytes)

		sID := uuid.New()
		uID := uuid.New()

		mock.ExpectHGet("sessions.id."+sID.String(), "sth").SetVal(tknStr)
		mock.ExpectHGet("sessions.id."+sID.String(), "uid").SetVal(uID.String())

		mock.ExpectDel("sessions.id." + sID.String()).SetVal(1)
		mock.ExpectDel("sessions.token." + tknStr).SetVal(1)
		mock.ExpectSRem("users."+uID.String()+".sessions", sID.String()).SetVal(1)

		err = ri.KillSession(context.Background(), sID)
		if err != nil {
			t.Error(err)
		}
	})

	t.Run("Trying to kill not existing session", func(t *testing.T) {
		sID := uuid.New()

		mock.ExpectHGet("sessions.id."+sID.String(), "sth").RedisNil()

		err := ri.KillSession(context.Background(), sID)
		if err != sessions.ErrSessionNotFound {
			t.Error("Fail: Trying to kill not existing session")
		}
	})
}

func TestKillAllSessions(t *testing.T) {
}

func TestGetAllSessions(t *testing.T) {

}

// One interface to test both
// session ID and user ID
type TokenSorUidAndToken struct {
	SorUid    string
	TokenHash string
	RealToken string
}

func RandomSorUidAndToken(count int) []TokenSorUidAndToken {
	var testcases []TokenSorUidAndToken

	for i := 0; i < count; i++ {
		tknBytes := make([]byte, sessions.TOKEN_BYTES_LENGTH, sessions.TOKEN_BYTES_LENGTH)
		_, err := rand.Read(tknBytes)
		if err != nil {
			return []TokenSorUidAndToken{}
		}
		tknStr := hex.EncodeToString(tknBytes)
		tknHashStr, err := sessions.HashToken(tknStr)
		if err != nil {
			panic(err)
		}
		newPair := TokenSorUidAndToken{
			SorUid:    uuid.New().String(),
			TokenHash: tknHashStr,
			RealToken: tknStr,
		}
		testcases = append(testcases, newPair)
	}
	return testcases
}

func TestGetUsrIdByToken(t *testing.T) {
	testcases := RandomSorUidAndToken(10)
	db, mock := redismock.NewClientMock()
	ri := sessions.NewRedisInterface(db)

	for _, testcase := range testcases {
		mock.ExpectHGet("sessions.token."+testcase.TokenHash, "uid").SetVal(testcase.SorUid)

		testName := fmt.Sprintf("Get User ID by token. %s", testcase.SorUid)
		t.Run(testName, func(t *testing.T) {
			uid, err := ri.GetUserIdByToken(context.Background(), testcase.RealToken)
			if err != nil {
				t.Error(err)
			}
			if uid.String() != testcase.SorUid {
				t.Errorf("Expected to get: %s. Got: %s", testcase.SorUid, uid.String())
			}
		})
	}

	testcases = RandomSorUidAndToken(1)
	testcase := testcases[0]

	mock.ExpectHGet("sessions.token."+testcase.TokenHash, "uid").RedisNil()
	t.Run("Get User ID of not existing token", func(t *testing.T) {
		uid, err := ri.GetUserIdByToken(context.Background(), testcase.RealToken)
		if err != nil && err != sessions.ErrSessionNotFound {
			t.Error(err)
		}
		if uid.String() != uuid.Nil.String() {
			t.Errorf("Expected to get nil id, but got %s", uid.String())
		}
	})
}
func TestGetSessionIdByToken(t *testing.T) {
	testcases := RandomSorUidAndToken(10)
	db, mock := redismock.NewClientMock()

	ri := sessions.NewRedisInterface(db)

	for _, testcase := range testcases {
		mock.ExpectHGet("sessions.token."+testcase.TokenHash, "sid").SetVal(testcase.SorUid)

		testName := fmt.Sprintf("Get Session ID by token. %s", testcase.SorUid)
		t.Run(testName, func(t *testing.T) {
			sid, err := ri.GetSessionIdByToken(context.Background(), testcase.RealToken)
			if err != nil {
				t.Error(err)
			}
			if sid.String() != testcase.SorUid {
				t.Errorf("Expected to get: %s. Got: %s", testcase.SorUid, sid.String())
			}
		})
	}

	testcases = RandomSorUidAndToken(1)
	testcase := testcases[0]

	mock.ExpectHGet("sessions.token."+testcase.TokenHash, "sid").RedisNil()
	t.Run("Get Session ID of not existing token", func(t *testing.T) {
		sid, err := ri.GetSessionIdByToken(context.Background(), testcase.RealToken)
		if err != nil && err != sessions.ErrSessionNotFound {
			t.Error(err)
		}
		if sid.String() != uuid.Nil.String() {
			t.Errorf("Expected to get %s, but got %s", uuid.Nil.String(), sid.String())
		}
	})
}
