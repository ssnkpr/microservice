package users

import (
	"context"
	"strings"

	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

type PostgresInterface struct {
	pool *pgxpool.Pool
}

// Creates new user
func (p *PostgresInterface) SaveUser(ctx context.Context, user UserCreationOpts) (uuid.UUID, error) {
	var uid uuid.UUID
	var err error

	passwordHash, salt, err := hashPassword(user.Password)
	if err != nil {
		return uuid.Nil, err
	}

	tx, err := p.pool.Begin(ctx)
	if err != nil {
		return uuid.Nil, err
	}

	err = tx.QueryRow(ctx, "INSERT INTO public.users DEFAULT VALUES RETURNING id;").Scan(&uid)
	if err != nil {
		tx.Rollback(ctx)
		return uuid.Nil, err
	}
	_, err = tx.Exec(ctx, "INSERT INTO public.creds (usrId, login, passwordHash, salt) "+
		"VALUES ($1, $2, $3, $4);", uid, user.Creds.Login, passwordHash, salt)
	if err != nil {
		tx.Rollback(ctx)
		// Check for dublicate
		if strings.Contains(err.Error(), "duplicate key value violates unique constrain") {
			return uuid.Nil, ErrLoginTaken
		}
		return uuid.Nil, err
	}
	_, err = tx.Exec(ctx, "INSERT INTO public.user_data (usrId, displayedName) VALUES ($1, $2)",
		uid, user.Name)
	if err != nil {
		tx.Rollback(ctx)
		return uuid.Nil, err
	}

	err = tx.Commit(ctx)
	if err != nil {
		return uuid.Nil, err
	}

	return uid, nil
}

// Deletes the user or returns an error
func (p *PostgresInterface) RemoveUser(ctx context.Context, id uuid.UUID) error {
	_, err := p.pool.Exec(ctx, "DELETE FROM public.users WHERE id = $1;", id)
	return err
}

func (p *PostgresInterface) GetUserByID(ctx context.Context, id uuid.UUID) (entities.User, error) {
	user := entities.User{}
	err := p.pool.QueryRow(ctx, "SELECT login FROM public.creds WHERE usrId = $1;",
		id).Scan(&user.Login)
	if err != nil {
		return user, err
	}
	err = p.pool.QueryRow(ctx, "SELECT displayedName FROM public.user_data WHERE usrId = $1;",
		id).Scan(&user.Name)
	if err != nil {
		return user, err
	}
	user.ID = id

	return user, nil
}

func (p *PostgresInterface) GetUserByLogin(ctx context.Context, login string) (entities.User, error) {
	user := entities.User{}
	err := p.pool.QueryRow(ctx, "SELECT usrId FROM creds WHERE login = $1;",
		login).Scan(&user.ID)
	if err != nil {
		return user, err
	}
	err = p.pool.QueryRow(ctx, "SELECT displayedName FROM public.user_data WHERE usrId = $1;",
		user.ID).Scan(&user.Name)
	if err != nil {
		return user, err
	}
	user.Login = login

	return user, nil
}

func (p *PostgresInterface) IsLoginTaken(ctx context.Context, login string) (bool, error) {
	var taken bool
	err := p.pool.QueryRow(ctx, "SELECT EXISTS(SELECT 1 FROM public.creds WHERE login = $1);",
		login).Scan(&taken)
	return taken, err
}

func (p *PostgresInterface) CheckCreds(ctx context.Context, creds entities.Creds) (uuid.UUID, bool, error) {
	var uid uuid.UUID
	var actualPasswordHash, salt []byte

	err := p.pool.QueryRow(ctx, "SELECT usrId, passwordHash, salt FROM public.creds WHERE login = $1;",
		creds.Login).Scan(&uid, &actualPasswordHash, &salt)
	if err != nil {
		if err == pgx.ErrNoRows {
			return uuid.Nil, false, nil
		}
		return uuid.Nil, false, err
	}

	valid := verifyPassword(creds.Password, salt, actualPasswordHash)
	return uid, valid, err
}

func (p *PostgresInterface) GetCountOfUsers(ctx context.Context) (int, error) {
	var count int
	err := p.pool.QueryRow(ctx, "SELECT COUNT(*) FROM public.users;").Scan(&count)
	if err != nil {
		return 0, err
	}
	return count, err
}

// TODO
func ChangeLogin(ctx context.Context, uID uuid.UUID, newLogin string) error {
	return nil
}

// TODO
func ChangePassword(ctx context.Context, uID uuid.UUID, newPassowrd string) error {
	return nil
}

// TODO
func ChangeDisplayedPassword(ctx context.Context, uID uuid.UUID, newPassowrd string) error {
	return nil
}
