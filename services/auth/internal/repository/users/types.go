package users

import (
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
)

// Options here
type UserCreationOpts struct {
	Name string
	entities.Creds
}
