package users

import (
	"fmt"
	"reflect"
	"strings"
	"testing"
)

type TestCases struct {
	Input string
	Hash  string
	Salt  string
}

func TestHashPassword(t *testing.T) {
	testErr := func(test string) error { return fmt.Errorf(fmt.Sprintf("Test %v failed: ", test)) }
	tc := map[string]TestCases{
		"alphabets": {
			Input: "qwerty",
			Hash:  "",
			Salt:  "",
		},
		"alphanumeric": {
			Input: "qwerty123",
			Hash:  "",
			Salt:  "",
		},
		"numeric": {
			Input: "123456",
			Hash:  "",
			Salt:  "",
		},
		"special": {
			Input: "!@#$%^&*()_+",
			Hash:  "",
			Salt:  "",
		},
		"ideal": {
			Input: "Qwerty123!@#$%^&*()_+",
			Hash:  "",
			Salt:  "",
		},
	}

	for test, testdata := range tc {
		t.Run(test, func(t *testing.T) {
			hash, salt, err := hashPassword(testdata.Input)
			if err != nil {
				t.Errorf("%v Error hashing password: %v", testErr(test), err)
			}
			if reflect.DeepEqual(hash, testdata.Hash) {
				t.Errorf("%v Hash is not as expected", testErr(test))
			}
			if reflect.DeepEqual(salt, testdata.Salt) {
				t.Errorf("%v Salt is not as expected", testErr(test))
			}
		})
	}
}

func TestVerifyPassword(t *testing.T) {
	testErr := func(test string) error { return fmt.Errorf(fmt.Sprintf("Test %v failed: ", test)) }
	tc := map[string]TestCases{
		"alphabets": {
			Input: "qwerty",
			Hash:  "",
			Salt:  "",
		},
		"alphanumeric": {
			Input: "qwerty123",
			Hash:  "",
			Salt:  "",
		},
		"numeric": {
			Input: "123456",
			Hash:  "",
			Salt:  "",
		},
		"special": {
			Input: "!@#$%^&*()_+",
			Hash:  "",
			Salt:  "",
		},
		"ideal": {
			Input: "Qwerty123!@#$%^&*()_+",
			Hash:  "",
			Salt:  "",
		},
	}

	for test, testdata := range tc {
		t.Run(test, func(t *testing.T) {
			inputs := strings.SplitN(testdata.Input, ",", 3)
			password, salt, inHash := inputs[0], inputs[1], inputs[2]
			valid := verifyPassword(password, []byte(salt), []byte(inHash))
			if !valid {
				t.Errorf("%v Password is not valid", testErr(test))
			}
		})
	}
}
