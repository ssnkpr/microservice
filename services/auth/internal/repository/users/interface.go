package users

import (
	"context"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/entities"
)

type DbInterface interface {
	SaveUser(context.Context, UserCreationOpts) (uID uuid.UUID, err error)
	RemoveUser(ctx context.Context, uID uuid.UUID) error

	GetUserByID(ctx context.Context, uID uuid.UUID) (entities.User, error)
	GetUserByLogin(ctx context.Context, login string) (entities.User, error)

	CheckCreds(context.Context, entities.Creds) (uID uuid.UUID, valid bool, err error)

	IsLoginTaken(ctx context.Context, login string) (isFree bool, err error)

	GetCountOfUsers(ctx context.Context) (count int, err error)
}

func NewDbInterface(pool *pgxpool.Pool) DbInterface {
	return &PostgresInterface{
		pool: pool,
	}
}
