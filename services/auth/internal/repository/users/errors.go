package users

import "errors"

var ErrUserNotFound = errors.New("User was not found")
var ErrLoginTaken = errors.New("Login is already taken")
var ErrNoChange = errors.New("No changes")
