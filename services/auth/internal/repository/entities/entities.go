package entities

import (
	"github.com/google/uuid"
)

type Creds struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type UserApiPostIn struct {
	Name     string `json:"name"`
	Login    string `json:"login"`
	Password string `json:"password"`
}

type Session struct {
	SID       uuid.UUID `json:"sessionID"`
	UID       uuid.UUID `json:"usrID"`
	CreatedAt int64     `json:"createdAt"`
	DeviceInf string    `json:"deviceInf"`
	CreatorIP string    `json:"creatorIP"`
}

type User struct {
	ID    uuid.UUID `json:"id"`
	Login string    `json:"login"`
	Name  string    `json:"name"`
}
