package main

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"sync"
	"time"

	httpApi "gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/http"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/sessions"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/users"
	"gitlab.com/4nd3rs0n/microservice/services/auth/pkg/dbtools"
	"gitlab.com/4nd3rs0n/microservice/services/auth/pkg/logger"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/redis/go-redis/v9"
)

// Config

var certPath = os.Getenv("CERT_PATH")

// TODO: CLI args, config, environment variables
var (
	sqlPath   = "sql"
	grpcPort  = 50051
	startGrpc = false
	httpPort  = 8000
	startHttp = true

	dbSslEnabled = false
)

func main() {
	lgr := logger.NewLogger(logger.DEFAULT_DEV...)
	if !startGrpc && !startHttp {
		lgr.Ftl("All interfaces disabled! Enable at least one interface to run the service")
	}
	ctx := context.Background()

	// Making random random
	rand.Seed(time.Now().UnixNano())

	dbUrl := fmt.Sprintf(
		"postgresql://%s:%s@%s:%s/%s",
		os.Getenv("POSTGRES_USER"), os.Getenv("POSTGRES_PASSWORD"),
		os.Getenv("POSTGRES_HOST"), os.Getenv("POSTGRES_PORT"),
		os.Getenv("POSTGRES_DB"),
	)
	if !dbSslEnabled && os.Getenv("STAGE") == "dev" {
		lgr.Wrn("PostgreSQL SSL disabled")
		dbUrl += "?sslmode=disable"
	}

	// Initialize connection
	pgxConn, err := pgx.Connect(ctx, dbUrl)
	if err != nil {
		log.Fatalf(err.Error())
	}

	lgr.Dbg("Initializing pgx connection")
	checkUpPgxOpts := dbtools.CheckUpOpts{
		Ctx:     ctx,
		Retries: 3,
		Timeout: 10 * time.Second,
		Pingable: &dbtools.PingAdptrPgSQL{
			Conn: pgxConn,
		},
		Logr:   &lgr,
		DBname: "PostgreSQL",
	}
	// Checking is DB up
	err = dbtools.CheckUp(checkUpPgxOpts)
	if err != nil {
		log.Fatalf(err.Error())
	}
	lgr.Dbg("Closing pgx connection")
	pgxConn.Close(ctx)

	// Apply Migrations
	dir, err := os.Getwd()
	if err != nil {
		lgr.Ftl("Failed to get current dirrectory, %w", err)
	}
	lgr.Inf("Applying Postgres migrations from %s/%s", dir, sqlPath)
	mOpts := dbtools.MigrationOpts{
		MSrc:    fmt.Sprintf("file://%s", sqlPath),
		DBurl:   dbUrl,
		VerLim:  0,
		DropDev: true,
		Logger:  &lgr,
	}

	_, _, err = dbtools.DoMigrate(mOpts)
	if err != nil {
		lgr.Ftl("Failed to apply database migrations: %s", err.Error())
	}

	lgr.Dbg("Initializing PostgreSQL pool")
	pgxPoolConf, err := pgxpool.ParseConfig(dbUrl)
	if err != nil {
		lgr.Ftl("Failed to parse pgx pool config")
	}

	pgPool, err := pgxpool.NewWithConfig(ctx, pgxPoolConf)
	if err != nil {
		lgr.Ftl("Failed to connect to PostgreSQL: %s\n\n%s", err.Error(),
			"Check is postgres up, check connection creds and try to restartserver.")
	}
	defer pgPool.Close()

	lgr.Dbg("Initializing redis client")
	redisClient := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%s",
			os.Getenv("REDIS_HOST"),
			os.Getenv("REDIS_PORT")),
		Password: os.Getenv("REDIS_PASSWORD"),
		DB:       0, // use default DB
	})

	checkUpRdsOpts := dbtools.CheckUpOpts{
		Ctx:     ctx,
		Retries: 3,
		Timeout: 10 * time.Second,
		Pingable: &dbtools.PingAdptrRds{
			Conn: redisClient,
		},
		Logr:   &lgr,
		DBname: "Redis",
	}
	// Checking is DB up
	err = dbtools.CheckUp(checkUpRdsOpts)
	if err != nil {
		log.Fatalf(err.Error())
	}

	lgr.Dbg("Initialising DB interfaces")
	UDBInterface := users.NewDbInterface(pgPool)
	SDBInterface := sessions.NewDbInterface(redisClient)

	wg := sync.WaitGroup{}

	if startGrpc {
		lgr.Inf("Starting gRPC API")
		wg.Add(1)
		go func() {
			lgr.Ftl("Err in gRPC API: %w",
				StartGrpcAPI(ctx, UDBInterface, SDBInterface, grpcPort))
			wg.Done()
			lgr.Wrn("gRPC API stopped")
		}()
	}

	// Setup and start http server
	if startHttp {
		lgr.Inf("Starting HTTP API")
		wg.Add(1)

		apiOpts := httpApi.HttpApiOpts{
			ApiPrefix: "/",
			Users:     UDBInterface,
			Sessions:  SDBInterface,
			Logger:    &lgr,
		}

		api, err := httpApi.NewApi(ctx, apiOpts)
		if err != nil {
			lgr.Ftl("Err while creating HTTP API instance %w", err)
		}
		go func() {
			err := http.ListenAndServe(fmt.Sprintf(":%d", httpPort), api)
			lgr.Ftl("Err in HTTP API: %w", err)
			wg.Done()
			lgr.Wrn("HTTP API stopped")
		}()
	}

	// Waiting until every interface stopped
	wg.Wait()
}
