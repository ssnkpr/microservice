package main

import (
	"context"
	"fmt"
	"net"

	grpcApi "gitlab.com/4nd3rs0n/microservice/services/auth/internal/api/grpc"
	pb "gitlab.com/4nd3rs0n/microservice/services/auth/internal/domain/proto"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/sessions"
	"gitlab.com/4nd3rs0n/microservice/services/auth/internal/repository/users"

	"google.golang.org/grpc"
)

func StartGrpcAPI(ctx context.Context, udbi users.DbInterface, sdbi sessions.DbInterface, port int) (err error) {
	// Initialising listener
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		err = fmt.Errorf("Failed to start gRPC API: Failed to listen on %d port: %s", port, err.Error())
		return
	}

	grpcServ := grpc.NewServer()

	serv := grpcApi.NewServer(ctx, sdbi, udbi)
	pb.RegisterAuthServer(grpcServ, &serv)

	// Starting server
	err = grpcServ.Serve(listener)
	if err != nil {
		err = fmt.Errorf("Failed to serve: %s", err.Error())
	}
	return
}
