BEGIN;

CREATE TABLE IF NOT EXISTS users (
  -- Table "users" stores only IDs.
  -- If there is need to store something
  -- else with user, you have to create
  -- another migration. And hopefully
  -- without changing this table, but
  -- creating another with reference to
  -- the user's ID
  id  UUID PRIMARY KEY DEFAULT gen_random_uuid() UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS creds (
  -- All credentials for the authorisation 
  -- are stored here.
  usrId        UUID REFERENCES users(id) ON DELETE CASCADE UNIQUE NOT NULL,
  login        TEXT NOT NULL UNIQUE,
  passwordHash BYTEA NOT NULL,
  salt         BYTEA NOT NULL
);

CREATE TABLE IF NOT EXISTS user_data (
  usrId         UUID REFERENCES users(id) ON DELETE CASCADE UNIQUE NOT NULL,
  displayedName TEXT
);

COMMIT;
